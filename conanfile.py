#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
from distutils.spawn import find_executable

from conans import AutoToolsBuildEnvironment, VisualStudioBuildEnvironment
from conans import ConanFile, tools
from conans.errors import ConanException


class QtConan(ConanFile):
    name = "quazip"
    version = "0.7.6"
    license = "LGPL-2.1"
    homepage = "https://stachenov.github.io/quazip/"
    url = "https://bitbucket.org/egorspk/conan-quazip"
    description = "Conan.io package for QuaZIP. Use only for windows and visual studio"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = "shared=True"
    source_dir = "quazip"
    generators = "visual_studio"

    def source(self):
        self.run("git clone https://github.com/stachenov/quazip.git")
        self.run("cd %s && git checkout %s" % (self.source_dir, self.version))

    def build(self):
        if self.settings.os != "Windows" or self.settings.compiler != "Visual Studio":
            raise ConanException("This recipe only for windows and visual studio")
        else:
            args = []
            if not self.options.shared:
                args.insert(0, "staticlib")
            if self.settings.build_type == "Debug":
                args.append("debug")
            else:
                args.append("release")
            self._build_msvc(args)

    def _build_msvc(self, args):
        # check qmake
        active_perl = find_executable("qmake.exe")
        if active_perl is None:
            raise ConanException("Unable find qmake in PATH")

        # check zlib paths
        if not "ZLIB_INCLUDE" in self.env:
            raise ConanException("ZLIB_INCLUDE environment variable is not set")
        zlib_include = self.env["ZLIB_INCLUDE"]

        if not "ZLIB_LIB" in self.env:
            raise ConanException("ZLIB_LIB environment variable is not set")
        zlib_lib = self.env["ZLIB_LIB"]

        env = {}
        env_build = VisualStudioBuildEnvironment(self)
        env.update(env_build.vars)

        with tools.environment_append(env):
            vcvars = tools.vcvars_command(self.settings)

            self.output.info("### Qmake ###")
            self.run("cd %s && qmake.exe \"CONFIG+=%s\" \"INCLUDEPATH+=%s\" \"LIBS+=-L%s -lzlib\""
                     % (self.source_dir, " ".join(args), zlib_include, zlib_lib))

            self.output.info("### Build ###")
            self.run("%s && cd %s && nmake.exe" % (vcvars, self.source_dir))

            self.output.info("### Install ###")
            self.run("%s && cd %s && nmake.exe install" % (vcvars, self.source_dir))

    def package(self):
        self.copy("*.h", dst="include/quazip", src="quazip/quazip")
        self.copy("*.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)

    def package_info(self):
        self.cpp_info.bindirs = ['bin']
        self.cpp_info.includedirs = ['include']
        self.cpp_info.libdirs = ['lib']

        if self.settings.build_type == "Debug":
            self.cpp_info.libs = ["quazipd"]
        else:
            self.cpp_info.libs = ["quazip"]
