# conan-quazip

The packages generated with this **conanfile** can be found in [bintray.com](https://bintray.com/spk/conan).

## Reuse the package

### Basic setup

```
conan remote add spk https://api.bintray.com/conan/spk/conan 
conan install quazip/0.7.6@spk/stable
```

To build a quazip, set the following environment variables: ZLIB_INCLUDE and ZLIB_LIB:

```
conan install quazip/0.7.6@spk/stable --build missing -e ZLIB_INCLUDE="C:\zlib\include" -e ZLIB_LIB="C:\zlib\lib"
```
